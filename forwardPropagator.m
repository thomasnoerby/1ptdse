function psi = forwardPropagator(uj,psi)
%     This method picks out the type of forward propagation to be used

    psi = splitStepForwardPropagation(uj,psi); % the Suzuki-Trotter method
%     psi = exactForwardPropagation(uj,psi); % The exact propagation (costly)
end
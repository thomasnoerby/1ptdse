function expecVal = calcExpecVal(u,x)
    global nt;
    
    psiList = forwardPropagation(u);
%     xPsiList = x'.*psiList;
    
    expecVal = zeros(1,nt);
    
    for i = 1:nt
        psi_i = psiList(:,i);
        val = overlap(psi_i,x'.*psi_i);
%         val = psiList(:,i)'*xPsiList(:,i);
        expecVal(1,i) = val;
    end
end
% run main.m til 119 or something
close all;

global nt dt x;

% load data
mainFolder = pwd;
% cd Runs/run13/T_9/seed5;
cd Runs/run19/T_10/seed44;
load uOpt.mat;
load T.mat;
u = uOpt;
cd(mainFolder);

% calculate relevant variables
nt = floor(T/dt)+1;
t = linspace(0, T, nt); % time interval

% calculate plot values
xDensity = calculateDensity(u);
Vdensity = potentialDensity(u);
Flist = forwardPropagationFList(u);
xExpec = calcExpecVal(u,x);


% figure and tight subplots
figure()
ha = tight_subplot(1,1,[.01 .03],[.15 .1],[.15 .1]); %
% set(ha(1:1),'XTickLabel','');
% set(ha(1:4),'YTickLabel','');
set(ha(1:1),'xlim', [min(t) max(t)])

% color maps map
myColorMap = hot(256); % colormap choice
myColorMap(1:50,:) = 1; % make 1:60 white (removes background)
colormap(myColorMap); % set colormap
% colorbar % put this at relevant plots if nedded

% subplot 1
axes(ha(1))

hold on
yyaxis left
ha(1).YAxis(1).Color = 'r';

imagesc(t,x,xDensity,'AlphaData',0.5)
% title('X density')
ylabel('$x$')

% subplot 2
% axes(ha(2))
hold on
plot(t,xExpec,'r--','DisplayName','$\langle x(t)\rangle$')
% title('Expectation value of x')
% ylabel('$\langle x\rangle$')
l = legend('Location','NW');

plot(t,u,'r-','DisplayName','$u(t)$')

% % subplot 3
% axes(ha(3))
% hold on
% imagesc(t,x,Vdensity,'AlphaData',0.5)
% % title('Potential density')
% ylabel('x')

% subplot 4
% axes(ha(2))
yyaxis right
ha(1).YAxis(2).Color = 'b';
Flist = 1-Flist;
semilogy(t,Flist,'b','DisplayName','$1-F(t)$')
xlabel('t')

% title('Infidelity')
xlim([min(t), max(t)])
set(gca,'yscale','log');
% ylabel('1-F')




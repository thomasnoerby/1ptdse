% run main.m until line 65 before this one
% fixes error with flist - it was not saved correctly before, but this one
% amends that error

global psiTar nt;

mainFolder = pwd;
runNr = 15;
nT = 11;
nSeeds = 50;

path = strcat("Runs/run",num2str(runNr),"/");
for i = 1:nT
    path_i = strcat(path,"T_",num2str(i),"/");
    
    for j = 1:nSeeds
        path_ij = strcat(path_i,"seed",num2str(j),"/");
        uOpt_ij = strcat(path_ij,"uOpt.mat");
        
        load(uOpt_ij);
        [~,nt] = size(uOpt);
        
        FList = fidelity(psiTar,forwardPropagationFinal(uOpt));
        save_ij = strcat(path_ij,"FList.mat");
        save(save_ij,"FList");
    end
end
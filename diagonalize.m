function [eigVec, eigVal] = diagonalize(u)
    H = makeHamiltonian(u);
    [eigVec, eigVal] = eig(H);
end
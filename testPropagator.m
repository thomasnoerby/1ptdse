function check = testPropagator(u)
    global psi0 nt x;
    
    psiExact = psi0; % current state
    for j = 1:nt-1
        uj = u(j);
        psiExact = exactForwardPropagation(uj,psiExact);
    end
    
    psiSplit = psi0; % current state
    for j = 1:nt-1
        uj = u(j);
        psiSplit = splitStepForwardPropagation(uj,psiSplit);
    end
    
    ratio = psiExact./psiSplit;
    
    %% removable - just a plot of the above
    fig1 = figure()
    
    plot(x,psiExact.*conj(psiExact),'r.-')
    hold on
    plot(x,psiSplit.*conj(psiSplit),'b.--')
    xlabel("t")
    ylabel("|psi|^2")
    legend("Exact","Split")
    savefig(fig1,"overlap")
    
    fig2 = figure()
    plot(x,psiExact,'r.-')
    hold on
    plot(x,psiSplit,'b.--')
    xlabel("t")
    ylabel("psi")
    legend("Exact","Split")
    savefig(fig2,"psi")
    %%
    
    check = 0; % should be 1 if the same, zero if not
end
clear all; close all; clc;

global nx x;
% define constants
t0 = 0;
T = 1000;
x0 = -1;
xMax = 1;
nt = 1024;
nx = 128;
dt = T/nt;
u0 = 0;

% intervals
t = linspace(t0, T, nt);
x = linspace(x0, xMax, nx);

V = makePotential(u0); % to plot
H = makeHamiltonian(u0); % temporary
[eigVec, eigVal] = diagonalize(u0);
p0 = eigVec(:,1)+eigVec(:,2)+eigVec(:,10);
p = p0;

figure()
hold on
plot(x,V)

f = plot(x,p.*conj(p));
set(f,'XData',x);

for j = 1:nt
    U = expm(-1i*H*dt);
    p = U*p;
    set(f,'YData',p.*conj(p));
    refreshdata(f,'caller');
    pause(0.01)
end
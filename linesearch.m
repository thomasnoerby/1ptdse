function [alpha,F] = linesearch(u,grad,alpha0)
    global psiTar;
    % This function calculates the optimal stepsize for the current
    % optimization of the control function
    f = @(alpha) -fidelity(psiTar,forwardPropagationFinal(u-alpha*grad')); % function to minimize
    [alpha,F] = fminsearch(f,alpha0);
    F = -F; % reversing the sign
end
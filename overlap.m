function o = overlap(p1,p2)
    global dx;
    o = p1'*p2; % the sum of all entries are normalized to one for each wavefunction, so no sqrt(dx) needed
    o = o*dx;
end

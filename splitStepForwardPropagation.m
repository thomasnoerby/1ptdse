function psi = splitStepForwardPropagation(u,psi)
    global dt kvec;

    uFrom = u(1);
    uTo = u(2);
    
    VFrom = makePotential(uFrom); % potential
    VTo = makePotential(uTo); % potential
    
    UvFrom = exp(-1i*VFrom'*dt/2); % ' is the transpose conjugate - V is real however, so it is okay
    UvTo = exp(-1i*VTo'*dt/2);
    
    Ut = exp(-1i*kvec.^2/2*dt); % kinetic term
    
    psi = UvFrom.*psi;
    psi = fft(psi);
    psi = Ut.*psi;
    psi = ifft(psi);
    psi = UvTo.*psi;
end
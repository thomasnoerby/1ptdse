function grad = numericalGradient(u)
    global nt psiTar;
    
    eps_diff = eps^(1/3); % constant for finite difference

    ej = zeros(1,nt); % j'th unit vector (1's will be filled in later)
    grad = zeros(1,nt); % numerical gradient, preallocation
    
    for j = 1:nt
        ej(1,j) = eps_diff; % setting unit vector and multiplying by constant (should only be used for this order of approximation)
        
        Fplus = fidelity(psiTar,forwardPropagationFinal(u+ej)); % fidelity of psiTarget and psiT with u+eps*ej
        Fminus = fidelity(psiTar,forwardPropagationFinal(u-ej)); % fidelity of psiTarget and psiT with u-eps*ej
        
        Fdiff = Fplus-Fminus;
        
        grad(1,j) = -Fdiff/(2*eps_diff); % central difference formula - the minus is also imbedded in gradient, so they are now on equal footing

        ej(1,j) = 0; % reset vector
    end
%     grad = shift(grad);
end

% function gradNew = shift(grad)
%     global nt;
%     gradNew = grad;
%     for j = 1:nt-1
%         gradNew(j+1) = (grad(j)+grad(j+1))/2;
%     end
%     gradNew(nt) = grad(nt)-(grad(nt)+grad(nt-1))/2;
% 
% end
function [f,g] = bfgsFuncWithGrad(u)
    % Function called by the bfgs optimization algorithm. The first output
    % is the function evaluated in u, and the second is the gradient.
    
    global psiTar;
    
    f = -fidelity(psiTar,forwardPropagationFinal(u));
    g = analyticalGradient(u);
end
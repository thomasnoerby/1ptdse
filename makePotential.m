function V = makePotential(u)
global x sigma Vdepth;
% V = 10*(x-u).^2; % perhaps make global, so it is easy to change

sigma = 1;
V = -Vdepth*exp(-(x-u).^2/(2*sigma^2));

%w =8;
%V = 0.5*w^2*(x-u).^2;
end
function [j,Flist,u] = grape(u,F)
    % This function performs the GRAPE algorithm on the control function u.
    % It returns the number of iterations required, j, a list of
    % fidelities, Flist, and the final control function, u.
    
    k = 1000; % number of iterations
    Ftresh = 0.99; % threshold
    alpha = 0.001; % initial step-size for Gradient Descent
    
    Flist = zeros(1,k); % list to store fidelities
    j = 1; % iteration
    
    for m = 1:k        
        % gradient
        grad = analyticalGradient(u); % calculate current gradient
            
        % control function
        [alpha,Fstep] = linesearch(u,grad,alpha); % optimal step size

        if(Fstep<F) % if the current point is a local maximum, finish grape
            disp('Local maximum found at fidelity')
            disp(F);
            j = m-1;
            return;       
        elseif(abs(Fstep-F)<eps) % if the optimization is inefficient
            disp('To small increments, returning')
            disp(F)
            return;
        end
        
        % step
        u = u-alpha*grad';

        % fidelity and tests
        F = Fstep;
        Flist(1,m) = F;
        j = m;
        
        if(F>Ftresh) % if the current fidelity is above the threshold, stop
            disp('Fidelity exceeded threshold')
            disp(F);
            return;
        end
    end
    
    disp('Maximum iterations have been run')
    disp(Flist(1,k))
end
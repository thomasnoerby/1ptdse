function psiList = forwardPropagation(u)
    % This function propagates the initial state, psi0, all the way to time
    % t = T with the current controlfunction
    global nt psi0;
    
    [n,~] = size(psi0); % number of rows (for preallocation)
    m = nt; % number of columns (for preallocation)
    
    psi = psi0; % current state
    psiList = zeros(n,m); % list of all psi's, preallocation
    psiList(:,1) = psi;
    
    for j = 1:nt-1
        uj = u(j:j+1);
        psi = forwardPropagator(uj,psi);
        psiList(:,j+1) = psi;
    end
end
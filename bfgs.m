function [F,u] = bfgs(u0)
    % This function uses an alternative optimization (MATLAB built-in). It
    % is implemented with a gradient, so it is faster and more precise.
    Ftresh = 0.99; % threshold
    maxFuncEval = 4000;
    maxIter = 1000;
    
    opt = optimoptions(@fminunc,'Display','off','ObjectiveLimit',-Ftresh,...
        'MaxFunctionEvaluations',maxFuncEval, ...
        'MaxIterations',maxIter); % options for optimization

    % setting problem
    problem.options = opt;
    problem.x0 = u0;
    problem.objective = @bfgsFuncWithGrad; % function to call
    problem.solver = 'fminunc'; % optimization algorithm
    
    [u,F,~,output] = fminunc(problem); % MATLAB built in unconstrained optimization function
    
    F = -F; % reversing the sign
    
    % test
    global outputVals;
    outs = [output.funcCount;output.iterations];
    outputVals = [outputVals, outs];
end


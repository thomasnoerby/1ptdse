function Flist = forwardPropagationFList(u)
    % This function propagates the initial state, psi0, all the way to time
    % t = T with the current controlfunction, and returns a list of all
    % fidelities along the way
    global nt psi0 psiTar;
    
    psi = psi0; % current state
    Flist = zeros(1,nt);
    Flist(1,1) = fidelity(psiTar,psi);
    for j = 1:nt-1
        uj = u(j:j+1);
        psi = forwardPropagator(uj,psi);
        Flist(1,j+1) = fidelity(psiTar,psi);
    end
end
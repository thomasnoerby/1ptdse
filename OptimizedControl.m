function u = OptimizedControl(nt)
%% run an averaged of all good controls
%     p = [];
    p2 = [];
%     load p.mat;
%     n = 9;
    load p2.mat;
%     p = p2;
    n = 20;
    u = zeros(1,nt);
    x = [1:nt];
    for i = 1:n
        u = u+p(i)*x.^(n-i);
    end
end
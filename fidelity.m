function f = fidelity(p1,p2)
    o = overlap(p1,p2);
    f = o*conj(o);
end
function density = calculateDensity(u)
    psiList = forwardPropagation(u);
    density = psiList.*conj(psiList);
end
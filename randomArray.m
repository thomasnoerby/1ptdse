function r = randomArray(scale)
    % This function creates a random row vector with length nt
    global nt;
    time = clock; % current time array
    rng_seed = time(5)*60+time(6);
    rng(rng_seed); % current minutes and seconds set as seed
    r = rand(1,nt)*scale;
end
function u = OptimizedControl2(nt)
%% run the specified control function as optimized control
    directory = "Runs\run13\T_1\seed45";
    cd(directory);
   
    load uOpt.mat;
    u = zeros(1,nt);
    [~,n] = size(uOpt);
    u(1:n) = uOpt;
    cd ..\..\..\..;
end
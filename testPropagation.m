function check = testPropagation(u)
    % This method tests the propagation methods by evolving a state
    % forwards and then backwards in time. The state at all times is then
    % compared    
    global nt;
    
    disp('Testing propagation methods') % Test message
    
    check = 1;
    maxDiff = eps^(1/3); % maximum allowed difference - eps is the machine epsilon 10-10
    
    psiList = forwardPropagation(u); % forward propagation
    psiT = psiList(:,end); % psi at t=T
    psiListReverse = backwardPropagationPsi(u,psiT); % backwards propagation
    
    for j = 1:nt
        psiForward = psiList(:,j);
        psiBackward = psiListReverse(:,j); % timesteps are arranged as psiForward
        F = fidelity(psiForward,psiBackward);
        check = abs(F-1)<maxDiff; % if true, check = 0 and the test fails
        if(check == 0)
            disp('Test failed') 
            return % return to caller
        end
    end
    
    disp('Test successful') % if the script gets here, the test is successful
end
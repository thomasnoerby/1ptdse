clear all; close all; clc;
tic
% remove these when tested
global outputVals;
outputVals = [];

global  nx nt x dx t dt Vdepth sigma psiTar psi0 kvec gradientChoice; % all global constants in the scripts

% gradientChoice
gradientChoice = 2; % decides on optimization algorithm. 1 is GRAPE, 2 is bfgs

% time definitions
t0 = 0; % initial time
T = 1; % final time
dt = 5e-3; % length of a time step 1e-3
nt = floor(T/dt)+1; % number of time slices (number of timesteps is nt-1)
t = linspace(t0, T, nt); % time interval

% spatial definitions
xMin = -3; 
xMax = 3;
L = xMax-xMin;
nx = 512; % number of x steps
x = linspace(xMin, xMax, nx); % x interval

dx = (x(2)-x(1)); % length of an x step

% momentum defintions
kvec = 2*pi/L*[0:nx/2 - 1, -nx/2:-1]'; % p = hbar * k, this is used instead of p

% initial potential and control function positions
A = -1; % position of initial potential
B = 1; % position of target potential
u0 = A; % initial control function position

% random array
randomScale = 1/3;
r = randomArray(randomScale);

% current controlfunction at all times (should be a method in itself, but is left here for easy accesibility)
u = A+(1-A/B)*sin(t/T*pi/2); % good control function
% u = OptimizedControl(nt);
% u = linspace(A,B,nt); % linear control function
% u = 0*t; % zero-control function
% u = r; % completely random control function
% u = u+r; % above with added random noise

% plot control function
uPlot = 1;
fig1 = figure(uPlot);
plot(t,u,'b--')
hold on
xlabel("t")
ylabel("u(t)")
title("Control function at all times")

% initial state
Vdepth = 130;
V = makePotential(u0); % initial state potential - only to plot
[eigVec, eigVal] = diagonalize(u0); % eigen-vectors and -values
eigVec = eigVec/sqrt(dx); % normalized eigenvectors
psi0 = eigVec(:,1); % ground state
psi = psi0; % current state = ground state

% target state
VTar = makePotential(B); % potential to plot
[eigVecTar,eigValTar] = diagonalize(B); % target eigenvectors and values
eigVecTar = eigVecTar/sqrt(dx); % normalized eigenvectors
psiTar = eigVecTar(:,1); % target state (eigenstate at position B)

% plot
figure()
hold on

% plot(x,V) % initial potential

h = plot(x,V,'k'); % current potential (tweezer)
plot(x,VTar,'k--') % target potential

axis([-3,3,-150,0])

yyaxis right % adds another y-axis to the right
plt = gca;
plt.YAxis(2).Color = 'r'; % change color of RHS y-axis to black

axis([-3,3,0,2])

wavefct_scalefactor = 1; % scales the wavefunctions for visual effect
plot(x,wavefct_scalefactor*psiTar.*conj(psiTar),'r--') % target groundstate
f = plot(x,wavefct_scalefactor*psi.*conj(psi),'r-'); % current state of the system (defined so it is possible to update)

xlabel("x [sim. units]")
yyaxis left
ylabel("Potential [sim. units]")

yyaxis right
ylabel("Wavefunction density [sim. units]")

% tests
% checkPropagation = testPropagation(u); % testing propagation methods
% assert(checkPropagation == 1);
% gradRatio = testGradient(u);
% checkPropagator = testPropagator(u);
% assert(checkPropagator == 1);

% evolution
for j = 1:nt-1
    uj = u(j:j+1); % control function at time tj
    
    psi = forwardPropagator(uj,psi); % wavefunction at time tj
    
    v = makePotential(uj(2)); % current potential (u(tj)) - only for plotting
    
    set(f,'YData',wavefct_scalefactor*(psi.*conj(psi))); % update state plot
    set(h,'YData',v); % update potential plot

    pause(0.01) % pause to animate 
end

F = fidelity(psi,psiTar)

%densityPlot(t,x,calculateDensity(u))


[k,Flist,uOpt] = Optimization(u,F);

Flist = [F, Flist]; % adding the fidelity without optimization

% plot fidelity climb
fig2 = figure();
semilogy(1-Flist(1:k+1),'b.-')
hold on
xlabel("Iteration, i")
ylabel("1-Fidelity")
% title("Fidelity at each iteration of optimization")
return;
savefig(fig2,"figures/Fidelity")
% set(gca,'xtick',1:k)


% updated control function plot
figure(uPlot)
plot(t,uOpt,'r-')
legend('$u_{init}$',"$u_{Opt}$",'Location','best')
savefig(fig1,"figures/Control_function")

return;
% F(T) curve
nSeeds = 50; % number of seeds run
nT = 11; % number of T's investigated - default 11
Tmin = 0.32; 
Tmax = 0.38;

% All the below could be a function (perhaps with smaller functions inside)
Ts = linspace(Tmin,Tmax,nT); % Final times to investigate

uFunc = @(T,tArray) A+(1-A/B)*sin(tArray/T*pi/2); % good control function as a function
scale = 1/2; % scale for random

F0 = 0; % zero fidelity for initializing optimization
Fs = zeros(nSeeds,nT); % array to save fidelities

[runNr, ~] = size(ls("Runs"));
runNr = runNr-1;
folderName = strcat("Runs\run",num2str(runNr));
mkdir(folderName)

u0 = [];

for m = 1:nT % loop over all times
    T = Ts(1,m); % current final time
    nt = floor(T/dt)+1; % new nt (global so permeates all functions)
    tArray = linspace(t0,T,nt); % new time array
    
%     u0 = uFunc(T,tArray); % control function for this final time
    % the below is usually just OptimizedControl(nt); - here i test
    % different things
    if(m==1)
        u0 = OptimizedControl2(nt);
    else
        u0 = OptimizedControl3(nt,m,runNr,nSeeds);
    end
%     u0 = OptimizedControl4(nt);
    
    newFolderName = strcat(folderName,"\T_",num2str(m));
    mkdir(newFolderName)
    
    for j=1:nSeeds % loop over seeds
        r = randomArray(scale); % random noise for the array
        u = u0+r; 
        
        [nIter,Flist,uOpt] = Optimization(u,F0);
        saveData(newFolderName,T,Flist(1:nIter),u,uOpt,scale); % save the values in the argument
        
        Fcur = Flist(nIter); % Fidelity from optimization
        Fs(j,m) = Fcur; % saving current fidelity
    end
end

% plot F(T) curve
FTplot = figure();
semilogy(Ts,1-Fs,'k.') % all seeds and final fidelities
hold on
semilogy(Ts,1-max(Fs),'r.') % best fidelites for each final time as red dots
semilogy([Tmin Tmax],[1e-2 1e-2],'k--') % goal line
axis([Tmin-0.1 Tmax+0.1 0.009 1])
xlabel("t")
ylabel("1-F")
saveName = strcat(folderName,"/FTplot");
savefig(FTplot,saveName)

% FTplot1-5 is with scale = 1/3, 6 is with 1/20, 7 is 1/200, 8 - 10 is
% 1/1000, 11 is 1/10, 12 is with 1

% FTplot_second_run_1 is with 1/20, 2 is with 1/15

% FTplot_long_1 is 1/30, 2 is with 1/50

% random: 1 is with 1/3, 2 is with 1/20, 3 is with 1/30
% runs 1-8  and all earlier figures are with dt = 0.01. All after are dt =
% 1e-3;

% run 9 is with BFGS with dt = 0.001 on standard control function. It never
% finished.

% run 10 is with gradient descent, dt = 0.001 should probably be deleted
% run 11 is with gradient descent, dt = 0.001, finished (Standard control)

% run 12 is with new optimized control , gradient descent, dt = 0.001

% run 13 is with dt = 5e-3, paused and must be continued later - new
% optimized control - merge run 13, 14 and 15, 16

% run 17 and on is bfgs. 17 is with low noise, 18 is with larger noise and
% maxFuncEval = maxIter = 1000, 19 is with large noise and no constraints
% (except maxFid), 20 is with maxFuncEval = 4000
toc

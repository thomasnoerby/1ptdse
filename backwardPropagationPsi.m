function psiList = backwardPropagationPsi(u,psiT)
    % This function propagates the state psi from t=T to t=0. This method
    % is used to check the validity of the propagation methods
    global nt;
    
    [n,~] = size(psiT); % number of rows (for preallocation)
    m = nt; % number of columns (for preallocation)
    
    psi = psiT; % current state
    psiList = zeros(n,m); % list of all psi's, preallocation
    psiList(:,end) = psi;
    
    for j = nt-1:-1:1
        uj = fliplr(u(j:j+1));
        psi = backPropagator(uj,psi);
        psiList(:,j) = psi;
    end
end
function chiList = backwardPropagation(u)
    global nt psiTar;
    
    [n,~] = size(psiTar); % number of rows (for preallocation)
    m = nt; % number of columns (for preallocation)
    
    chi = psiTar; % |chi(T)>
    chiList = zeros(n,m); % list of all chi's, preallocation
    chiList(:,end) = chi; % placed at the end, as this is at t = T
    
    for j = nt-1:-1:1      
        uj = fliplr(u(j:j+1));
        chi = backPropagator(uj,chi);
        chiList(:,j) = chi; % fill from the end to the start
    end
end
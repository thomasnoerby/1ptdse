function u = OptimizedControl4(nt)
%% update each new T
    m = 11;
    runNr = 17;
    nSeeds = 50;
    j = findPreviousMax(runNr,nSeeds,m);
    directory = strcat("Runs\run17\T_11\seed",num2str(j));
    
    cd(directory);
   
    load uOpt.mat;
    u = uOpt(1:nt);
    cd ..\..\..\..;
end
function Vdensity = potentialDensity(u)
    global nx nt;
    Vdensity = zeros(nx,nt);
    for i = 1:nt
        V_i = makePotential(u(i))';
        Vdensity(:,i) = V_i;
    end
end
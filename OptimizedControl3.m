function u = OptimizedControl3(nt,m,runNr,nSeeds)
%% update each new T
    m = m-1;
    j = findPreviousMax(runNr,nSeeds,m);
    directory = strcat("Runs\run15\T_",num2str(m),"\seed",num2str(j));
    
    cd(directory);
   
    load uOpt.mat;
    u = zeros(1,nt);
    [~,n] = size(uOpt);
    u(1:n) = uOpt;
    cd ..\..\..\..;
end
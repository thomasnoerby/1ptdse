function grad = analyticalGradient(u)
    % This method calculates the user defined gradient (as opposed to
    % matlabs own function)
    global dt nt psiTar; 
    
    % state lists
    psiList = forwardPropagation(u); % prepare list of psi's at all times
    chiList = backwardPropagation(u); % prepare list of chi's at all times
        
    grad = zeros(nt,1); % list of all gradients, preallocation
    psiT = psiList(:,end); % psi(t=T)
    o = -1i*overlap(psiT,psiTar); % overlap between psiTarget and psiT (the final psi)
    
    for j = 1:nt
        uj = u(j); % control function at tj
        chi = chiList(:,j);
        psi = psiList(:,j);
        
        gradV = analyticPotentialGradient(uj); % analytic gradient of the potential
        gradV = gradV'; % transposed to match psi dimensions (x is a row vector, which makes gradV a row)
        
        expecVal = overlap(chi,gradV.*psi); % total expectation value  
        grad_i = real(-2*dt*o*expecVal); % gradient for the current time-step, minus to make it gradient descent
        grad(j,1) = grad_i;
    end
end
function psi = backPropagator(uj,psi)
%     This method picks out the type of forward propagation to be used

    psi = splitStepBackPropagation(uj,psi); % the Suzuki-Trotter method
%     psi = exactBackPropagation(uj,psi); % The exact propagation (costly)
end
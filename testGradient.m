function ratio = testGradient(u)
    global t;

    anGrad = analyticalGradient(u)'; % analytical gradient
    numGrad = numericalGradient(u); % numerical gradient

    ratio = numGrad./anGrad;
    
    %% just a plot, can be deleted
    fig = figure();
    plot(t,numGrad,'ro')
    
    hold on
    plot(t,anGrad,'b.')
    xlabel("t")
    ylabel("Gradient")
    
    legend("NumGrad","AnGrad")
    savefig(fig,"figures/testGradient")
    
    fig1 = figure();
    semilogy(1-anGrad./numGrad,'.')
    hold on
    ylabel("1-anGrad/numGrad")
    xlabel("Index of gradients")
    savefig(fig1,"figures/gradRatio")
end
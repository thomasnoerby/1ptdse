clear all; close all; clc;

dt = 5e-3; 
% load data
mainFolder = pwd;
cd Runs/run13/T_9/seed5;
load uOpt.mat;
load T.mat;
u = uOpt;
cd(mainFolder);

nt = floor(T/dt)+1;
t = linspace(0, T, nt); % time interval

newT = 0.32;
new_nt = floor(newT/dt)+1;
new_t = linspace(0,newT,new_nt*1000);

s = spline(t,u,t);
new_s = spline(t,u,new_t);

figure()
hold on
plot(t,u,'b-')
plot(t,s,'r--')
plot(new_t,new_s,'-.')


function psi = exactBackPropagation(uj,psi)
    global dt;

    H_c = makeHamiltonian(uj); % Hamiltonian with current u at time ti
    U = expm(+1i*H_c*dt); % simple time evolution (should be changed to split-step method)
    psi = U*psi; % evolved state
end
clear all; close all; clc;

runNr = 19;
nSeeds = 50;
maxT = 0.38;
jMax = controlClusterPlots(runNr,nSeeds,maxT);
save('jMax.mat','jMax');



% Flist = [];
% scale = [];
% u0 = [];
% uOpt = [];
% T = [];
% 
% load Flist.mat;
% load scale.mat;
% load u0.mat;
% load uOpt.mat;
% load T;
% 
% individualPlots(Flist,u0,uOpt,T);

function jMax = controlClusterPlots(runNr,nSeeds,maxT)
    uOpt = []; % predefined
    Flist = []; % perhaps load from a folder with specific run properties saved
    
    mainFolder = pwd;
    
    runFolder = strcat("..\Runs\run",num2str(runNr));
    ls(runFolder)
    [nT, ~] = size(ls(runFolder))
    nT = nT-3; % remove . .. and fig
    
%     uPlot = 1;
    
    
%     Fmin = [0,0.8,0.9,0.95,0.98,0.99];
%     plotPrepare(uPlot,Fmin);    

%     figure(uPlot)
%     xlabel("x")
%     title("Control function at each t, filtered by fidelity")
%     hold on
%     ylabel("Control functions")
    
%     nt = maxT*10000+1; % really bad practice - 1000 should be 1/dt or something
%     umean = zeros(1,31);
%     count = 0;
    
%     umean = zeros(1,nt);
%     count = umean;
    

    jMax = zeros(1,nT);
    
    for i = 1:nT
        Tfolder = strcat(runFolder,"\T_",num2str(i));
        Fmax = 0;
        
        for j = 1:nSeeds

            seedFolder = strcat(Tfolder,"\seed",num2str(j));
            cd(seedFolder);
            
            load u0.mat;
            load uOpt.mat;
            load FList.mat;
            cd(mainFolder);
            
            Fmax_i = max(FList);
            if(Fmax_i>Fmax)
                Fmax = Fmax_i;
                jMax(i) = j;
            end
%             
%             plotSorter(uOpt,FList,Fmin,uPlot)
%             
%             if(max(FList)>0.95)
% %                 figure(uPlot)
% %                 plot(uOpt,'-')
% %                 plot(u0,'--')
%                 [~,n] = size(uOpt);
%                 umean(1:n) = umean(1:n)+uOpt(1:n);
%                 count(1:n) = count(1:n)+1;
%             end
            
%             cd(mainFolder);
        end
    end
%     umean = umean./count;
%     
%     figure()
%     plot(umean,'-')
%     x = [1:nt];
%     p = polyfit(x,umean,9);
%     y = polyval(p,x);
%     hold on
%     plot(x,y)
%     xlabel("t")
%     ylabel("u(t)")
%     legend("avg. control function","fit",'location','NW')
end
clear all; close all; clc;

path = "..\Runs\run13";
nT = 11;
nSeeds = 50;

Tmin = 0.32;
Tmax = 0.38;

Ts = linspace(Tmin,Tmax,nT);

Fs = zeros(nSeeds,nT);
cd(path);

for i = 1:nT
    path_i = strcat("T_",num2str(i),"\");
    cd(path_i);
    for j = 1:nSeeds
        seed_i = strcat("seed",num2str(j),"\");
        cd(seed_i);
        load Fmax.mat;
        Fs(j,i) = Fmax;
        cd ..
    end
    cd ..
end

cd ..
cd ..

FTplot = figure();

semilogy(Ts,1-Fs,'k.') % all seeds and final fidelities
hold on
semilogy(Ts,1-max(Fs),'r.') % best fidelites for each final time as red dots
semilogy([Tmin Tmax],[1e-2 1e-2],'k--') % goal line
axis([Tmin-0.1 Tmax+0.1 0.009 1])
xlabel("t")
ylabel("1-F")
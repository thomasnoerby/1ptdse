clear all; close all; clc;



runNr = 3;
nSeeds = 50;
maxT = 0.38;
controlClusterPlot(runNr,nSeeds,maxT);



% Flist = [];
% scale = [];
% u0 = [];
% uOpt = [];
% T = [];
% 
% load Flist.mat;
% load scale.mat;
% load u0.mat;
% load uOpt.mat;
% load T;
% 
% individualPlots(Flist,u0,uOpt,T);

function controlClusterPlot(runNr,nSeeds,maxT)
    uOpt = []; % predefined
    Flist = []; % perhaps load from a folder with specific run properties saved
    
    mainFolder = pwd;
    
    runFolder = strcat("..\Runs\run",num2str(runNr));
    
    [nT, ~] = size(ls(runFolder));
    nT = nT-3; % remove . .. and fig
    
    uPlot = 1;
    
    
    Fmin = [0,0.8,0.9,0.95,0.98,0.99];
    plotPrepare(uPlot,Fmin);    

    figure(uPlot)
%     xlabel("x")
%     title("Control function at each t, filtered by fidelity")
    hold on
%     ylabel("Control functions")
    subplot(2,3,1)
    ylabel("Control function")
    subplot(2,3,4)
    ylabel("Control function")
    xlabel("t")
    subplot(2,3,5)
    xlabel("t")
    subplot(2,3,6)
    xlabel("t")
    
    nt = maxT*1/5e-3+1; % really bad practice - 1000 should be 1/dt or something
%     umean = zeros(1,31);
%     count = 0;
    
    umean = zeros(1,nt);
    count = umean;
    utest = [];
    
    for i = 1:nT
        Tfolder = strcat(runFolder,"\T_",num2str(i));
        
        for j = 1:nSeeds
            seedFolder = strcat(Tfolder,"\seed",num2str(j));
            cd(seedFolder);
            
            load u0.mat;
            load uOpt.mat;
            load FList.mat;
            cd(mainFolder);
            
            plotSorter(uOpt,FList,Fmin,uPlot)
            
            if(max(FList)>0.95)
%                 figure(uPlot)
%                 plot(uOpt,'-')
%                 plot(u0,'--')
                [~,n] = size(uOpt);
                umean(1:n) = umean(1:n)+uOpt(1:n);
                count(1:n) = count(1:n)+1;
                utest=uOpt;
            end
            
            cd(mainFolder);
        end
    end
    umean = umean./count;

    
    figure()
    plot(umean,'-')
    x = [1:nt];
    p = polyfit(x,umean,30);
    y = polyval(p,x);
    hold on
    plot(x,y,'--')
    xlabel("t")
    ylabel("u(t)")
    legend("avg. control function","fit",'location','NW')
end
clear all; close all; clc;

mainFolder = pwd;
runNr = 19;
nT = 11;
nSeeds = 50;

path = strcat("../Runs/run",num2str(runNr),"/");
for i = 1:nT
    path_i = strcat(path,"T_",num2str(i),"/");
    Fmin = [];
    Fmax = [];
    
    for j = 1:nSeeds
        path_ij = strcat(path_i,"seed",num2str(j),"/");
        FList_ij = strcat(path_ij,"FList.mat");
        
        load(FList_ij);
        [~,nt] = size(FList);
        
        Fmin = [Fmin, 1-FList(1)];
        Fmax = [Fmax, 1-FList(end)];
    end
    
    figure()
    [~,edges] = histcounts(log10([Fmax,Fmin]));
    
    histogram(Fmin,10.^edges)
    hold on
    histogram(Fmax,10.^edges)
    
    set(gca, 'xscale','log')
    xlabel("1-F")
    ylabel("#seeds")
end
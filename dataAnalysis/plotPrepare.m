function plotPrepare(figNr,Fmin)
    figure(figNr)
    xlabel('Index')
    hold on
    ylabel('Control function')

    subplot(2,3,1)
    title('All control functions')

    [~,n] = size(Fmin);
    
    for i = 1:n
        titleStr = strcat(num2str(Fmin(i)),'$<$F');
        if i<n
            titleStr = strcat(titleStr,'$<$',num2str(Fmin(i+1)));
        end
        if i==n
            titleStr = strcat('F$>$',num2str(Fmin(i)));
        end
        subplot(2,3,i)
        hold on
        title(titleStr)
    end
end
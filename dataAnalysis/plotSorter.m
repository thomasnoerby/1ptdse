function plotSorter(uOpt,FList,Fmin,figNr)
    F = max(FList);
    
    [~,n] = size(Fmin);
    
    figure(figNr)

    for i = 1:n
        F_i = Fmin(i);
        if((F>F_i)&&(i<n)&&(F<Fmin(i+1)))
            subplot(2,3,i)
            hold on
            plot(uOpt,'-')
        elseif(i==n&&F>F_i)
            subplot(2,3,i)
            hold on
            plot(uOpt,'-')
        end
    end

end
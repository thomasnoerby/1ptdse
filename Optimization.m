function [j,Flist,u] = Optimization(u,F)
    % This function chooses between optimization methods. 1 is GRAPE, 2 is
    % BFGS.
    global gradientChoice;
    
    if(gradientChoice == 1)
        [j,Flist,u] = grape(u,F);
    elseif(gradientChoice == 2)
        [Flist,u] = bfgs(u);
        j = 1; % bfgs only returns the best fidelity
    end
end
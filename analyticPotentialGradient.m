function gradV = analyticPotentialGradient(u)
    global x sigma;
    V = makePotential(u);
    gradV = (x-u)/sigma^2.*V;% provide analytical function
end
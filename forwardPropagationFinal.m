function psi = forwardPropagationFinal(u)
    % This function propagates the initial state, psi0, all the way to time
    % t = T with the current controlfunction, and returns psi(t=T)
    global nt psi0;
    
    psi = psi0; % current state
    for j = 1:nt-1
        uj = u(j:j+1);
        psi = forwardPropagator(uj,psi);
    end
end
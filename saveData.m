function saveData(folderName,T,FList,u0,uOpt,scale)
% This function creates a folder for the specific run and saves FList, u0,
% uOpt and scale in that folder
    global gradientChoice;
    mainFolder = pwd;

    cd(folderName)
    [n, ~] = size(ls);
    n = n-2; % corrects for . and ..
    next = n+1; % new folder index

    folder = strcat("seed",num2str(next)); % folder name
    mkdir(folder); % creating folder
    cd(folder); % entering folder

    % saving files
    save('Flist.mat','FList');
    save('u0.mat','u0');
    save('uOpt.mat','uOpt');
    save('scale.mat','scale');
    save('T.mat','T');
    save('gradientChoice.mat','gradientChoice')

    cd(mainFolder); % return to main folder
end
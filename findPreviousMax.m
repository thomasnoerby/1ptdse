function jMax = findPreviousMax(runNr,nSeeds,m)
    uOpt = []; % predefined
    Flist = []; 

    mainFolder = pwd;

    runFolder = strcat(pwd,"\Runs\run",num2str(runNr));

    jMax = 0;

    Tfolder = strcat(runFolder,"\T_",num2str(m));
    Fmax = 0;

    for j = 1:nSeeds
        seedFolder = strcat(Tfolder,"\seed",num2str(j));
        cd(seedFolder);

        load u0.mat;
        load uOpt.mat;
        load FList.mat;
        cd(mainFolder);

        Fmax_i = max(FList);
        if(Fmax_i>Fmax)
            Fmax = Fmax_i;
            jMax = j;
        end
    end
end